import {Routes, RouterModule} from '@angular/router';
import {PupilResultComponent} from './PupilResult/pupilResult';
import {LoginComponent} from './Login/login';
import {AddPupilComponent} from './AddPupil/addPupil';
import {ProfileComponent} from './profile/profile.component';
import {AddTeacherComponent} from './Admin/add-teacher/add-teacher.component';
import {DecreeOverviewComponent} from './Pupil/decree-overview/decree-overview.component';
import {AddSchoolComponent} from './Admin/add-school/add-school.component';
import {AddMarkComponent} from './Teacher/add-mark/add-mark.component';
import {AddMarkFormComponent} from './Teacher/add-mark-form/add-mark-form.component';

const appRoutes: Routes = [
  {path: 'pupil', component: PupilResultComponent},
  {path: 'login', component: LoginComponent},
  {path: 'addPupil', component: AddPupilComponent},
  {path: 'addTeacher', component: AddTeacherComponent},
  {path: 'pupil/norm', component: DecreeOverviewComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'addSchool', component: AddSchoolComponent},
  {path: 'addMark', component: AddMarkComponent},
  {path: 'addMarkForm', component: AddMarkFormComponent}
];
export const routing = RouterModule.forRoot(appRoutes);
