import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecreeOverviewComponent } from './decree-overview.component';

describe('DecreeOverviewComponent', () => {
  let component: DecreeOverviewComponent;
  let fixture: ComponentFixture<DecreeOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecreeOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecreeOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
