import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './addPupil.html',
  styleUrls: ['./addPupil.css']
})
export class AddPupilComponent implements OnInit {
  title = 'SportPrediction';

  constructor(private http: HttpClient) {
  }

  schoolList: School[];

  ngOnInit(): void {
    this.getSchool();
  }

  getSchool() {
    this.http.get('http://localhost:5001/adminPanel/school').subscribe((res: any) => {
      this.schoolList = res;
    });

  }


}

class School {
  id;
  name;
}
