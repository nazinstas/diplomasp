import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { routing } from './app-routing.module';
import { AppComponent } from './app.component';
import {PupilResultComponent} from './PupilResult/pupilResult';
import {LoginComponent} from './Login/login';
import {AddPupilComponent} from './AddPupil/addPupil';
import {HttpClientModule} from '@angular/common/http';
import { AddMarkComponent } from './Teacher/add-mark/add-mark.component';
import { AddTeacherComponent } from './Admin/add-teacher/add-teacher.component';
import { DecreeOverviewComponent } from './Pupil/decree-overview/decree-overview.component';
import { AddSchoolComponent } from './Admin/add-school/add-school.component';
import { ProfileComponent } from './profile/profile.component';
import { AddMarkFormComponent } from './Teacher/add-mark-form/add-mark-form.component';

@NgModule({
  declarations: [
    AppComponent,
    PupilResultComponent,
    LoginComponent,
    AddPupilComponent,
    AddMarkComponent,
    AddTeacherComponent,
    DecreeOverviewComponent,
    AddSchoolComponent,
    AddPupilComponent,
    ProfileComponent,
    AddMarkFormComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
